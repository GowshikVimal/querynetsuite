package jdbcdriver.example1;

import com.netsuite.jdbcx.openaccess.OpenAccessDataSource;

import java.io.InputStream;
import java.lang.RuntimeException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public abstract class JdbcExample {

	public static final String PROP_FILE_NAME = "connection.properties";
	private static final String TYPE_DS = "-DS";
	private static final String TYPE_CS = "-CS";

	public static void main(String[] args) throws Exception {

		// validate input parameters
		if (args.length != 2) {
			System.out.println("Usage:");
			System.out.println();
			System.out.println("To run Datasource example:");
			System.out.println("	run.[bat|sh] -DS \"query\"");
			System.out.println();
			System.out.println("To run ConnectionString example:");
			System.out.println("	run.[bat|sh] -CS  \"query\"");
			return;
		}

		boolean useDatasource;
		if (TYPE_DS.equals(args[0])) {
			useDatasource = true;
		} else if (TYPE_CS.equals(args[0])) {
			useDatasource = false;
		} else {
			System.out.println(String.format("First parameter must be '%s' or '%s'", TYPE_CS, TYPE_DS));
			return;
		}

		String query = args[1];

		// load connection properties
		Properties properties = new Properties();
		InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(PROP_FILE_NAME);
		properties.load(in);
		in.close();

		String serverHost = properties.getProperty("ServerHost");
		String serverDataSource = properties.getProperty("DataSource");
		String login = properties.getProperty("Login");
		String password = properties.getProperty("Password");
		String accountId = properties.getProperty("AccountId");
		int roleId = convertToNumber(properties.getProperty("RoleId"), "RoleID");
		int port = convertToNumber(properties.getProperty("Port"), "Port");

		System.out.println("Running with serverHost = " + serverHost);
		System.out.println("Running with port = " + port);
		System.out.println("Running with serverDataSource = " + serverDataSource);
		System.out.println("Running with login = " + login);
		System.out.println("Running with password = ***");
		System.out.println("Running with accountId = " + accountId);
		System.out.println("Running with roleId = " + roleId);

		// initiate connection
		Connection connection = null;

		if (useDatasource) {
			// create the JDBC datasource and perform initial setup
			OpenAccessDataSource sds = new OpenAccessDataSource();
			sds.setServerDataSource(serverDataSource);
			sds.setServerName(serverHost);
			sds.setPortNumber(port);
			sds.setCustomProperties(String.format("(AccountID=%s;RoleID=%d)", accountId, roleId));
			sds.setEncrypted(1);
			// Set the NegotiateSSLClose parameter to false so connection.close() is processed immediatelly
			sds.setNegotiateSSLClose("false");
			connection = sds.getConnection(login, password);
		} else {
			Class.forName("com.netsuite.jdbc.openaccess.OpenAccessDriver");
			// Set the NegotiateSSLClose parameter to false so connection.close() is processed immediatelly
			String connectionString = String.format("jdbc:ns://%s:%d;ServerDataSource=%s;encrypted=1;CustomProperties=(AccountID=%s;RoleID=%d);NegotiateSSLClose=false",
													serverHost, port, serverDataSource, accountId, roleId);
			System.out.println("Using connection string: " + connectionString);
			connection = DriverManager.getConnection(connectionString, login, password);
		}


		Statement statement = null;
		ResultSet resultSet = null;
		try {
			// execute query
			statement = connection.createStatement();
			System.out.println("Executing query: " + query);
			resultSet = statement.executeQuery(query);

			System.out.println("Retrieving data...");
			List<String> columnNames = getColumnNames(resultSet);
			printList(columnNames);

			// process results
			while (resultSet.next()) {
				List<Object> row = new ArrayList<Object>();
				// do something meaning full with the result
				for (int i = 0; i < columnNames.size(); i++) {
					row.add(resultSet.getObject(i + 1));
				}

				printList(row);
			}
		}
		finally {
			cleanResources(resultSet, statement, connection);
		}

	}

	// Utility methods
	private static int convertToNumber(String value, String propertyName)
	{
		try {
			return Integer.valueOf(value);
		} catch (NumberFormatException e) {
			throw new RuntimeException(propertyName + " must be a number: " + value);
		}
	}

	private static List<String> getColumnNames(ResultSet resultSet) throws SQLException {
		List<String> columnNames = new ArrayList<String>();
		ResultSetMetaData metaData = resultSet.getMetaData();
		for (int i = 0; i < metaData.getColumnCount(); i++) {
			// indexing starts from 1
			columnNames.add(metaData.getColumnName(i + 1));
		}
		return columnNames;
	}

	private static void printList(List<?> values) {
		System.out.println();
		for (int i = 0; i < values.size(); i++) {
			System.out.print(values.get(i));
			if (i != values.size()) {
				System.out.print(",");
			}
		}
	}

	private static void cleanResources(ResultSet resultSet, Statement statement, Connection connection) {
		if (resultSet != null) {
			try {
				resultSet.close();
			}
			catch (SQLException e) {
			}
		}
		if (statement != null) {
			try {
				statement.close();
			}
			catch (SQLException e) {
			}
		}
		if (connection != null) {
			try {
				connection.close();
			}
			catch (SQLException e) {
			}
		}
	}

}
