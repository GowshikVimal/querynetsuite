Make sure Java Development Kit (JDK) version 7 or higher is installed on your system.
Make sure 'javac' and 'jar' executables are on your path (e.g. by including JAVA_HOME\bin directory on your path).