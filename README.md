# README #

This README would normally document whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* Helps to Access Netsuite Database and View data through SQL Queries from the terminal of our local system


### How do I get set up? ###

* Make sure Java SE 7 or higher is installed on the system
* Make sure java[.exe] is on PATH
* Edit configuration in connection.properties to match account (Production/Sandbox)
* Execute sh run.[sh] -DS "SQL Query"

### Guidelines ###

* Server Host for Netsuite Sandbox : 4149447-sb1.connect.api.netsuite.com
* AccountId for Sandbox : 4149447_SB1
